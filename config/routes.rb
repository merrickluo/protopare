# frozen_string_literal: true

Rails.application.routes.draw do
  get '/graphql', to: 'playground#index', only: [:index]
  post '/graphql', to: 'graphql#execute'

  get '/', to: redirect('/app/characters')
  get '/app', to: redirect('/app/characters')
  get '/app/*path', to: 'app#index'

  resources :spells, only: [:show]
end
