# frozen_string_literal: true

module Services
  module Crawler
    class SpellList
      LINKS = [
        { class_name: 'mage',
          url: 'https://trpgtdnd.weebly.com/27861241072786134899.html' }
      ].freeze

      def call
        LINKS.each do |link|
          resp = Faraday.get(link[:url])
          spell_list_urls = ::Parsers::SpellList.parse(resp.body)
          spell_list_urls.each do |url|
            ::Services::Crawler::Spell.new.call(link[:class_name], url)
          end
        end
      end
    end
  end
end
