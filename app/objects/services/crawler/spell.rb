# frozen_string_literal: true

module Services
  module Crawler
    class Spell
      def call(class_name, url)
        resp = Faraday.get(url)
        spell_params = ::Parsers::Spell.parse(resp.body)
        p spell_params
        # ::Spell.create(spell_params.merge(class_name: class_name))
      end
    end
  end
end
