# frozen_string_literal: true

module Services
  class Import::Spells
    def call
      xlsx = Roo::Spreadsheet.open('lib/assets/5E Spell List.xlsx')
      ::Spell.transaction do
        xlsx.sheet(0).drop(1).each do |row|
          full_name = row[1].sub('’', '\'') # fix regex
          name = full_name[/[^\x00-\x7F]+/]
          name_en = full_name[/[[:ascii:]]+/].strip
          if ::Spell.find_by(name: name)
            p "#{name} #{name_en} already imported"
            next
          end

          spell = ::Spell.create(
            level: row[0],
            name: name,
            name_en: name_en,
            school: row[2],
            casting_time: row[3],
            range: row[4],
            component: row[5],
            duration: row[6],
            concentration: !row[7].blank?,
            save_throw: row[8],
            ritual: !row[9].blank?,
            materials: row[10],
            description: row[11],
            upgrade: row[12]
          )
          klass_names = row.slice(13, row.size).reject(&:nil?)
          p "import spell #{name} for #{klass_names.join('')}"
          spell.klasses << ::Klass.where(name: klass_names)
        end
      end
    end
  end
end
