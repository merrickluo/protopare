# frozen_string_literal: true

module Services
  module Import
    class Specialities
      CATEGORY_PAGE = '分类:5E专长'
      SUBCAT_PAGE = '分类:种族专长'
      BASE_URL = 'https://wiki.aleadea.com/api.php'

      def call
        main_pages = fetch_list(CATEGORY_PAGE)
        sub_pages = fetch_list(SUBCAT_PAGE)

        pages = (main_pages + sub_pages).reject { |p| p['type'] == 'subcat' }
        Speciality.transaction do
          pages.each do |page|
            page_key = page['pageid'].to_s
            content = fetch_page(page['title'])[page_key]
            next if Speciality.find_by(name: content['title'])

            extract = content['extract']
            name = content['title']
            name_en = extract[/(?<=\(|（)[[:ascii:]]+(?=\)|）)/]

            p "import speciality #{name} (#{name_en}) "
            Speciality.create!(
              name: name,
              name_en: name_en,
              content: extract
            )
          end
        end
      end

      private

      def fetch_list(list_page)
        fetch(
          action: 'query',
          list: 'categorymembers',
          cmtitle: list_page,
          cmlimit: 500,
          cmprop: 'ids|title|type'
        )['query']['categorymembers']
      end

      def fetch_page(page)
        fetch(
          action: 'query',
          prop: 'extracts',
          titles: page
        )['query']['pages']
      end

      def fetch(params)
        JSON.parse(Faraday.get(BASE_URL, params.merge(format: 'json'))&.body)
      rescue Faraday::Error => e
        p "error fetching #{page}: #{e}"
        raise e
      end
    end
  end
end
