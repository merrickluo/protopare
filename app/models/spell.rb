# frozen_string_literal: true

class Spell < ApplicationRecord
  has_and_belongs_to_many :klasses
  scope :in_klass, ->(name) { joins(:klasses).where('klasses.name = ?', name) }
end
