class Character < ApplicationRecord
  belongs_to :race
  belongs_to :klass

  has_many :spells
end
