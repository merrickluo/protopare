# frozen_string_literal: true

class CharactersController < ApplicationController
  def index
    render component: 'CharacterPage'
  end
end
