module.exports = {
  theme: {
    extend: {
      colors: {
        dark: '#0863A5',
        darker: '#004290',
        light: '#97E0FF',
        lighter: '#e1f4fc',
        v: '#4b841c',
        s: '#b98100',
        m: '#77afc7',
        vt: '#1F3F11',
        st: '#6D360A',
        mt: '#033E6B',
      },
      width: {
        '7': '1.75rem',
        '7/10': '70%',
        '9/10': '90%',
      },
      maxWidth: {
        '8xl': '88rem',
      },
      inset: {
        '-1': '-0.25rem',
      },
      maxHeight: {
        '0': '0',
        imp: '1000px',
      },
    },
    transitionProperty: {
      all: 'all',
      'max-h': 'max-height',
    },
    fontFamily: {
      'han-serif': ['source-han-serif-sc'],
    },
  },
  variants: {
    borderWidth: ['first', 'last'],
    borderRadius: ['first', 'last'],
    height: ['hover'],
    maxHeight: ['hover'],
  },
  plugins: [require('tailwindcss-transitions')()],
};
