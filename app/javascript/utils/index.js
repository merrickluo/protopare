export const SPELL_LEVELS = [
  '戏法',
  '一环',
  '二环',
  '三环',
  '四环',
  '五环',
  '六环',
  '七环',
  '八环',
  '九环',
];

export const displaySpellLevel = level => {
  return SPELL_LEVELS[level];
};

export const convertSpellLevel = levelStr => {
  return SPELL_LEVELS.indexOf(levelStr);
};
