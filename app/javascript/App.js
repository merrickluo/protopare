import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import SpellPage from './components/SpellPage';
import SpecialityPage from './components/SpecialityPage';
import CharacterPage from './components/CharacterPage';

import Navbar from './components/Navbar';
const client = new ApolloClient({
  uri: '/graphql',
});

const routes = [
  {
    path: '/app/characters',
    name: '角色',
    component: CharacterPage,
  },
  {
    path: '/app/spells',
    name: '法术',
    component: SpellPage,
  },
  {
    path: '/app/specialities',
    name: '专长',
    component: SpecialityPage,
  },
];

const actions = [{ name: '登录' }, { name: '注册' }];
const App = () => {
  return (
    <ApolloProvider client={client}>
      <Router>
        <div className="w-full flex-col flex items-center">
          <Navbar
            routes={routes}
            actions={actions}
            currentRoute={'/app/characters'}
          />
          <div className="w-9/10 max-w-8xl">
            <Switch>
              {routes.map(route => (
                <Route
                  key={route.path}
                  path={route.path}
                  component={route.component}
                />
              ))}
            </Switch>
          </div>
        </div>
      </Router>
    </ApolloProvider>
  );
};

export default App;
