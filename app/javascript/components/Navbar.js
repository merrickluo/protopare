import React from 'react';
import PropTypes from 'prop-types';

import { useLocation } from 'react-router';
import { Link } from 'react-router-dom';

const Navbar = ({ routes, actions }) => {
  const currentRoute = useLocation().pathname;

  return (
    <nav className="flex w-full items-center justify-between bg-indigo-600">
      <div className="flex items-center p-4">
        <h1 className="text-white text-2xl">D20</h1>
        <div className="pl-4">
          {routes.map(route => (
            <Link
              key={route.path}
              to={route.path}
              className={`ml-4 text-sm ${
                currentRoute == route.path ? 'text-white' : 'text-gray-100'
              }`}
            >
              {route.name}
            </Link>
          ))}
        </div>
      </div>
      <div className="pr-4">
        {actions.map(action => (
          <button key={action.name} className="btn btn-dark ml-4">
            {action.name}
          </button>
        ))}
      </div>
    </nav>
  );
};

Navbar.propTypes = {
  routes: PropTypes.array,
  actions: PropTypes.array,
};

export default Navbar;
