import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';

const CHARACTER_QUERY = gql`
  query getCharacters {
    characters {
      name
      name_en
      klass
    }
  }
`;

const CharacterPage = () => {
  const { loading, error, data } = useQuery(CHARACTER_QUERY);

  if (loading) {
    return <div>Loading</div>;
  }
  return <div>CharacterPage</div>;
};

export default CharacterPage;
