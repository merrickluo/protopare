import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

const SpecialityList = ({ data }) => {
  const groups = _.chunk(data.specialities, data.specialities.length / 3);
  return (
    <div style={styles.table}>
      {groups.map(group => (
        <div key={group[0].id} style={styles.column}>
          {group.map(speciality => {
            const rawContent = { __html: speciality.content };
            return (
              <div key={speciality.id} style={styles.card}>
                <div dangerouslySetInnerHTML={rawContent}></div>
              </div>
            );
          })}
        </div>
      ))}
    </div>
  );
};

SpecialityList.propTypes = {
  data: PropTypes.object,
};

const styles = {
  card: {
    marginTop: 16,
  },
  extraSubtitle: {
    marginTop: 4,
  },
  table: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  column: {
    width: '32%',
  },
};

export default SpecialityList;
