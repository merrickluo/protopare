import React from 'react';
import PropTypes from 'prop-types';
import Tippy from '@tippy.js/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';

const Tooltip = props => {
  return (
    <Tippy {...props}>
      <div className="relative">
        {props.children}
        {!props.enabled && (
          <div className="absolute -top-1 -right-1 text-xs w-3 h-3 overflow-none">
            <FontAwesomeIcon icon={faInfoCircle} />
          </div>
        )}
      </div>
    </Tippy>
  );
};

Tooltip.propTypes = {
  enabled: PropTypes.bool,
  children: PropTypes.node,
};

export default Tooltip;
