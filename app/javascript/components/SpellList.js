import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { useInView } from 'react-intersection-observer';

import useWindowSize from './hooks/useWindowSize';
import SpellCard from './SpellCard';

const MAX_WIDTH = 1408;

const SpellList = ({ filter }) => {
  // TODO: handle error
  const { loading, data, fetchMore } = useQuery(SPELL_LIST_QUERY, {
    variables: { ...filter },
  });
  const size = useWindowSize();
  // 64 is container padding
  const width = _.min([size.width * 0.9, MAX_WIDTH]) - 16;
  const columns = _.floor(width / 16 / 28);
  const [hasMore, setHasMore] = useState(true);
  const [loadingRef, shouldLoadMore] = useInView({ threshold: 0.1 });

  let groups = [];
  if (data && data.spellSlice && data.spellSlice.spells) {
    groups = groupSpells(data.spellSlice.spells, columns);
  }

  // reset hasMore when filter change
  useEffect(() => {
    setHasMore(true);
  }, [filter]);

  useEffect(() => {
    if (loading || !shouldLoadMore || !data || !data.spellSlice) {
      return;
    }
    fetchMore({
      fetchPolicy: 'cache-and-network',
      variables: {
        ...filter,
        offset: data.spellSlice.paging.offset,
        limit: 20,
      },
      updateQuery: (previous, { fetchMoreResult }) => {
        if (!fetchMoreResult) {
          return previous;
        }
        if (fetchMoreResult.spellSlice.paging.count === 0) {
          setHasMore(false);
        }
        return Object.assign(
          {},
          {
            ...previous,
            spellSlice: {
              ...previous.spellSlice,
              spells: [
                ...previous.spellSlice.spells,
                ...fetchMoreResult.spellSlice.spells,
              ],
              paging: fetchMoreResult.spellSlice.paging,
            },
          },
        );
      },
    });
  }, [loading, data, shouldLoadMore]);

  return (
    <div className="flex flex-col h-full overflow-y-auto">
      <div className="flex justify-between">
        {groups.map((spells, index) => (
          <div key={index}>
            {spells.map(spell => (
              <div
                key={`spell-${spell.id}`}
                className="flex justify-between w-full"
              >
                <SpellCard spell={spell} />
              </div>
            ))}
          </div>
        ))}
      </div>
      {hasMore && <div ref={loadingRef}>Loading</div>}
    </div>
  );
};

const groupSpells = (spells, columnCount) => {
  // first group by row
  const rows = _.chunk(spells, columnCount);
  // then join it so we can render as columns
  const columns = _.zip(...rows);
  // finnally, filter out nulls
  return columns.map(column => _.compact(column));
};

SpellList.propTypes = {
  filter: PropTypes.object,
};

const SPELL_LIST_QUERY = gql`
  query GetSpellSlice(
    $klass: String
    $school: String
    $level: Int
    $search: String
    $limit: Int
    $offset: Int
  ) {
    spellSlice(
      filter: {
        klassName: $klass
        school: $school
        level: $level
        search: $search
      }
      paging: { limit: $limit, offset: $offset }
    ) {
      spells {
        id
        name
        description
        level
        school
        castingTime
        range
        component
        nameEn
        duration
        concentration
        ritual
        saveThrow
        materials
        upgrade
      }
      paging {
        offset
        count
      }
    }
  }
`;

export default SpellList;
