import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faTimes } from '@fortawesome/free-solid-svg-icons';

import { SPELL_LEVELS, convertSpellLevel } from '../utils';

const SPELL_FILTER_QUERY = gql`
  query spellFilters {
    klasses {
      name
    }
    spellSchools
  }
`;

const Select = ({ label, value, options, onChange }) => {
  return (
    <div className="flex flex-col items-start mr-2">
      <label className="tracking-wide text-xs font-bold pl-1">{label}</label>
      <div className="relative">
        <select
          className="focus:border-dark focus:outline-none outline-none block appearance-none bg-transparent w-32 border-b-2 border-light py-1 pt-2"
          value={value}
          onChange={onChange}
        >
          <option value="">全部</option>
          {options &&
            options.map(value => <option key={value}>{value}</option>)}
        </select>
        <div className="pointer-events-none absolute top-0 right-0 px-2 pt-2">
          <FontAwesomeIcon icon={faChevronDown} />
        </div>
      </div>
    </div>
  );
};

const SpellFilter = ({ onFilterChange }) => {
  const { loading, error, data } = useQuery(SPELL_FILTER_QUERY);
  const [filter, setFilter] = useState({
    search: '',
  });

  const handleFilterChange = key => event => {
    let value = event.target.value;
    const nfilter = { ...filter, [key]: value };
    setFilter(nfilter);
    onFilterChange({ ...nfilter, level: convertSpellLevel(nfilter.level) });
  };

  // yeah, it's ugly
  const handleClearSearch = () => {
    handleFilterChange('search')({
      target: {
        value: '',
      },
    });
  };

  const dataLoaded = !loading && !error;

  return (
    <form className="flex justify-between p-2">
      <div className="flex">
        <Select
          label="职业"
          value={filter.klass}
          options={dataLoaded && data.klasses.map(it => it.name)}
          onChange={handleFilterChange('klass')}
        />
        <Select
          label="学派"
          value={filter.school}
          options={dataLoaded && data.spellSchools}
          onChange={handleFilterChange('school')}
        />
        <Select
          label="等级"
          options={dataLoaded && SPELL_LEVELS}
          onChange={handleFilterChange('level')}
        />
      </div>
      <div className="flex flex-col items-start">
        <label className="text-xs font-bold">搜索</label>
        <div className="relative pt-2">
          <input
            className="border-b-2 border-light"
            type="text"
            placeholder="搜索法术中、英文名"
            value={filter.search}
            onChange={handleFilterChange('search')}
          />
          <div
            className="absolute right-0 top-0 px-2 pt-2"
            onClick={handleClearSearch}
          >
            <FontAwesomeIcon icon={faTimes} />
          </div>
        </div>
      </div>
    </form>
  );
};

SpellFilter.propTypes = {
  onFilterChange: PropTypes.func,
};

export default SpellFilter;
