import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Tippy from '@tippy.js/react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faInfoCircle,
  faChevronDown,
  faChevronUp,
  faShareSquare,
} from '@fortawesome/free-solid-svg-icons';

import Tooltip from './Tooltip';
import { displaySpellLevel } from '../utils';

const Tag = ({ children }) => (
  <div className="mx-1 font-bold text-lg text-white border-2 border-light rounded-lg px-1 bg-dark">
    {children}
  </div>
);
Tag.propTypes = {
  children: PropTypes.node,
};

const Attribute = ({ name, value }) => {
  // match castingtime and duration
  const match = value.match(/^(\d+)(\S+)/);
  const throwMatch = value.match(/(\S+)\/(\S+)/);

  // only show tooltip if text are truncated
  const valueRef = useRef(null);
  const [tooltipEnabled, setToolTipEnabled] = useState(false);
  useEffect(() => {
    const elem = valueRef.current;
    if (elem) {
      setToolTipEnabled(elem.scrollWidth != elem.offsetWidth);
    }
  }, [valueRef]);

  return (
    <div className="w-1/4 flex flex-col text-center text-dark border-r-2 border-gray-100 last:border-r-0">
      <p className="h-auto bg-light">{name}</p>
      {throwMatch && (
        <div className="bg-white h-full pt-1 text-dark">
          <p>{throwMatch[1]}</p>
          <p>{throwMatch[2]}</p>
        </div>
      )}
      {match && (
        <Tippy
          content={value || ''}
          enabled={tooltipEnabled}
          placement="bottom"
        >
          <div className="bg-white text-dark pb-1">
            <p className="text-2xl leading-tight">{match[1]}</p>
            <div className="flex items-center justify-center">
              <p className="truncate-none text-sm font-regular" ref={valueRef}>
                {_.trimEnd(_.trimStart(match[2], '个'), '）')}
              </p>
              {tooltipEnabled && (
                <FontAwesomeIcon
                  className="text-sm w-3 h-3 overflow-none"
                  icon={faInfoCircle}
                />
              )}
            </div>
          </div>
        </Tippy>
      )}
      {!match && !throwMatch && (
        <div className="flex h-full bg-white items-center justify-center">
          <Tippy content={value} enabled={tooltipEnabled} placement="bottom">
            <div className="flex w-full items-center justify-center px-2">
              <p className="text-dark text-lg truncate-none" ref={valueRef}>
                {value}
              </p>
              {tooltipEnabled && (
                <FontAwesomeIcon
                  className="text-sm w-3 h-3 overflow-none"
                  icon={faInfoCircle}
                />
              )}
            </div>
          </Tippy>
        </div>
      )}
    </div>
  );
};
Attribute.propTypes = {
  name: PropTypes.string,
  value: PropTypes.string,
};

const SpellCard = ({ spell, expand = false }) => {
  const [expanded, setExpanded] = useState(expand);

  const handleToggleExpanded = () => {
    setExpanded(!expanded);
  };

  const handleShareClick = () => {
    window.open(`/spells/${spell.id}`, '_blank');
  };

  return (
    <div className="mb-2 max-w-md mt-2 pt-1 rounded-lg h-full overflow-hidden font-bold shadow-lg p-6 bg-darker w-full">
      <div className="w-full flex justify-end text-white">
        <div
          className="pt-1 pb-1 px-2 text-large leading-none cursor-pointer"
          onClick={handleShareClick}
        >
          <FontAwesomeIcon icon={faShareSquare} />
        </div>
      </div>
      <div
        className="flex items-center justify-between"
        onClick={handleToggleExpanded}
      >
        <div className="flex flex-col">
          <div className="flex leading-none items-baseline">
            <h1 className="font-bold text-3xl text-white">{spell.name}</h1>
            <div className="text-white text-lg px-2">
              <FontAwesomeIcon icon={expanded ? faChevronUp : faChevronDown} />
            </div>
            {spell.concentration && <Tag>专</Tag>}
            {spell.ritual && <Tag>仪</Tag>}
          </div>
          <p className="text-white mr-2">{spell.nameEn}</p>
        </div>
        <p className="ml-2 text-xl text-black bg-lighter rounded px-2">
          {displaySpellLevel(spell.level)}
        </p>
      </div>
      <div className="flex mt-4 justify-between items-top">
        <div className="rounded-lg border-dark border-2 flex w-7/10 overflow-hidden">
          <Attribute name="需时" value={spell.castingTime || ''} />
          <Attribute name="持续" value={spell.duration || ''} />
          <Attribute name="范围" value={spell.range || ''} />
          <Attribute name="豁免" value={spell.saveThrow || '无'} />
        </div>
        <div className="flex flex-col items-end p-1 text-center justify-between font-bold text-white">
          <p className="text-lg font-bold text-black bg-lighter rounded px-2">
            A {spell.school}
          </p>
          <div className="flex text-center font-extrabold text-xl select-none">
            {spell.component.includes('V') && (
              <p className="bg-v text-vt rounded-full w-8 h-8">V</p>
            )}
            {spell.component.includes('S') && (
              <p className="bg-s text-st rounded-full w-8 h-8 ml-2">S</p>
            )}
            {spell.component.includes('M') && (
              <Tooltip content={spell.materials || ''}>
                <p className="bg-m text-mt rounded-full w-8 h-8 ml-2 cursor-pointer">
                  M
                </p>
              </Tooltip>
            )}
          </div>
        </div>
      </div>
      <div
        className={`font-regular text-white transition-all ${
          expanded ? 'pt-4 opacity-100 max-h-imp' : 'pt-0 opacity-0 max-h-0'
        }`}
      >
        <p>{spell.description}</p>
      </div>
    </div>
  );
};

SpellCard.propTypes = {
  spell: PropTypes.object,
  expand: PropTypes.bool,
};

export default SpellCard;
