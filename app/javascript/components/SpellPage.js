import React, { useState } from 'react';

import SpellList from './SpellList';
import SpellFilter from './SpellFilter';

const SpellPage = () => {
  const [filter, setFilter] = useState({});

  return (
    <>
      <SpellFilter onFilterChange={setFilter} />
      <SpellList filter={filter} />
    </>
  );
};

export default SpellPage;
