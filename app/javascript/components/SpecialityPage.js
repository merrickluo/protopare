import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';

import SpecialityList from './SpecialityList';

const SPELL_LIST_QUERY = gql`
  query GetSpecitilities {
    specialities {
      id
      name
      nameEn
      content
    }
  }
`;

const SpecialityPage = () => {
  const { loading, error, data } = useQuery(SPELL_LIST_QUERY);
  if (loading) {
    return <div>Loading</div>;
  }
  if (error) {
    return <div>Error</div>;
  }
  return <SpecialityList data={data} />;
};

export default SpecialityPage;
