# frozen_string_literal: true

module Types
  class Race < BaseObject
    field :id, Integer, null: false
    field :name, String, null: false
    field :name_en, String, null: false
  end
end
