# frozen_string_literal: true

module Types
  class Character < Types::BaseObject
    field :id, Integer, null: false
    field :name, String, null: true
    field :name_en, String, null: true
    field :klass, Klass, null: true
    field :race, Race, null: true
    field :spells, [Spell], null: true
  end
end
