# frozen_string_literal: true

module Types
  class Speciality < Types::BaseObject
    field :id, Integer, null: false
    field :name, String, null: true
    field :name_en, String, null: false
    field :content, String, null: true
  end
end
