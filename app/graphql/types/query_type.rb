# frozen_string_literal: true

module Types
  class QueryType < Types::BaseObject
    field :spell_slice, SpellSlice, null: false do
      argument :paging, PagingQuery, required: false
      argument :filter, SpellFilter, required: false
    end

    field :specialities, [Speciality], null: false
    field :klasses, [Klass], description: 'query klasses', null: false
    field :races, [Race], description: 'query races', null: false
    field :spell_schools, [String], null: false
    field :characters, [Character], null: false

    def spell_slice(args = {})
      filter, paging = args.values_at(:filter, :paging)

      # apply filter
      spells = ::Spell.order(level: :asc, id: :asc)
      unless filter[:klass_name].blank?
        spells = spells.in_klass(filter[:klass_name])
      end
      unless filter[:school].blank?
        spells = spells.where(school: filter[:school])
      end

      unless filter[:level].blank? || filter[:level].negative?
        spells = spells.where(level: filter[:level])
      end

      unless filter[:search].blank?
        spells = spells.where(
          'spells.name LIKE :search OR LOWER(spells.name_en) LIKE LOWER(:search)',
          search: "%#{filter[:search]}%"
        )
      end

      offset = paging[:offset] || 0
      limit = paging[:limit] || 20
      spells = spells.limit(limit).offset(offset)
      {
        spells: spells,
        paging: { offset: offset + spells.count, count: spells.count }
      }
    end

    # TODO: login required
    def characters
      ::Character.all
    end

    def klasses
      ::Klass.all
    end

    def races
      ::Race.all
    end

    def spell_schools
      # HACK: maybe setup a model for school
      ::Spell.select(:school).all.map(&:school).uniq
    end

    def specialities
      ::Speciality.all
    end
  end
end
