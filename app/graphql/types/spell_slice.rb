# frozen_string_literal: true

module Types
  class SpellSlice < BaseObject
    field :paging, Paging, null: false
    field :spells, [Spell], null: false
  end
end
