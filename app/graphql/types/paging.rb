# frozen_string_literal: true

module Types
  class Paging < BaseObject
    field :offset, Integer, null: false
    field :count, Integer, null: false
  end
end
