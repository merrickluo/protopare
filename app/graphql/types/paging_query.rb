# frozen_string_literal: true

module Types
  class PagingQuery < BaseInputObject
    argument :offset, Integer, required: false
    argument :limit, Integer, required: false
  end
end
