# frozen_string_literal: true

module Types
  class Spell < Types::BaseObject
    field :id, Integer, null: false
    field :level, Integer, null: false
    field :name, String, null: true
    field :name_en, String, null: false
    field :school, String, null: false
    field :casting_time, String, null: true
    field :range, String, null: false
    field :component, String, null: false
    field :duration, String, null: true
    field :concentration, Boolean, null: false
    field :save_throw, String, null: true
    field :ritual, Boolean, null: false
    field :materials, String, null: true
    field :description, String, null: true
    field :upgrade, String, null: true
  end
end
