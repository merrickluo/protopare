# frozen_string_literal: true

module Types
  class SpellFilter < BaseInputObject
    argument :level, Integer, required: false
    argument :klass_name, String, required: false
    argument :school, String, required: false
    argument :ritual, Boolean, required: false
    argument :concentration, Boolean, required: false
    argument :search, String, required: false
  end
end
