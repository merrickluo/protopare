# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

## Ruby

### install rbenv

`pacman -S rbenv ruby-build` or `brew install rbenv ruby-build`

### install ruby 2.6.3

`rbenv install 2.6.3`

make sure using 2.6.3 in work dir

`rbenv versions`

### install bundler

`gem install bundler`

### install dependencies

`bundle`
`yarn`

## System dependencies

### postgresql

`docker run --restart=always -d -p 127.0.0.1:5432:5432 postgres`

## Configuration

use default postgres config

## Database creation

`rails db:create db:migrate`

## Database initialization

`rails import:spell_list import:specialities import:races`

## start development

`rails s`
