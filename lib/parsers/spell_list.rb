# frozen_string_literal: true

module Parsers
  module SpellList
    BASE_URL = 'https://trpgtdnd.weebly.com/'

    def self.parse(html)
      document = Nokogiri::HTML(html)
      link_elems = document.css('div.paragraph font a')
      link_elems.map do |elem|
        "#{BASE_URL}#{elem.attribute('href').value}"
      end.uniq
    end
  end
end
