module Parsers
  module Spell
    def self.parse(html)
      document = Nokogiri::HTML(html)
      level = document.title[/(?<=\()\d/].to_i
      name = document.title[/(?<=\ )\S+/]
      content = document.css('div.paragraph font')

      content.traverse do |node|
        next if node.children.count.positive?

        print node.text if node.name != 'br'
        print "\n" if node.name == 'br'
      end
    end
  end
end
