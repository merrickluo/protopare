# frozen_string_literal: true

namespace :import do
  desc 'load stuff from excel'
  task spell_list: :environment do
    ::Services::Import::Spells.new.call
  end

  task specialities: :environment do
    ::Services::Import::Specialities.new.call
  end

  task races: :environment do
    names = %w(精灵 半精灵 人类 半身人 矮人 龙裔 侏儒 半兽人 提夫林)
    names_en = %w(Elf Half-Elf Human Halfling Dwarf Dragonborn Gnome Half-Orc Tiefling)
    names.zip(names_en).each do |name, name_en|
      p "import #{name}(#{name_en})"
      ::Race.find_or_create_by(name: name, name_en: name_en)
    end
  end
end
