# frozen_string_literal: true

require 'parsers/spell_list.rb'

namespace :crawler do
  desc 'fetch spells from trpgtdnd'

  task spells: :environment do
    Services::Crawler::SpellList.new.call
  end
end
