# frozen_string_literal: true

class ServiceGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('../templates', __dir__)
  argument :actions, type: :array, default: [], banner: 'private actions'

  def create_service_file
    template 'service.erb', File.join('app/objects/services', class_path, "#{file_name}.rb")
  end

  def create_service_spec_file
    template 'service_spec.erb', File.join('spec/objects/services', class_path, "#{file_name}_spec.rb")
  end
end
