FROM ruby:2.6.5

ARG RAILS_ENV=production
ARG RAILS_MASTER_KEY=

RUN apt-get update -qq && apt-get install -y postgresql-client
RUN mkdir /app
WORKDIR /app
COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock
RUN bundle install

RUN curl -sL https://deb.nodesource.com/setup_11.x | bash - && \
    apt-get install nodejs
RUN npm install -g yarn

COPY package.json /app/package.json
COPY yarn.lock /app/yarn.lock
RUN yarn install

COPY . /app

RUN RAILS_MASTER_KEY=${RAILS_MASTER_KEY} \
    RAILS_ENV=${RAILS_ENV} \
    rails assets:precompile

# Add a script to be executed every time the container starts.
# COPY scripts/startup.sh /usr/bin/
# RUN chmod +x /usr/bin/startup.sh
# ENTRYPOINT ["startup.sh"]

EXPOSE 3000

ENV RAILS_ENV=${RAILS_ENV}
ENV RAILS_MASTER_KEY=${RAILS_MASTER_KEY}
ENV DB_PASSWORD=

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
