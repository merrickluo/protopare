# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Klasses
Klass.create(name: '法师', name_en: 'Wizard')
Klass.create(name: '诗人', name_en: 'Bard')
Klass.create(name: '牧师', name_en: 'Cleric')
Klass.create(name: '德鲁伊', name_en: 'Druid')
Klass.create(name: '圣武士', name_en: 'Paladin')
Klass.create(name: '游侠', name_en: 'Ranger')
Klass.create(name: '术士', name_en: 'Sorcerer')
Klass.create(name: '邪术士', name_en: 'Warlock')
