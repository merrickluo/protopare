class CreateSpecialities < ActiveRecord::Migration[5.2]
  def change
    create_table :specialities do |t|
      t.string :name
      t.string :name_en
      t.text :description

      t.timestamps
    end
  end
end
