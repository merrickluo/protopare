class CreateKlasses < ActiveRecord::Migration[5.2]
  def change
    create_table :klasses do |t|
      t.string :name, index: true
      t.string :name_en, index: true

      t.timestamps
    end
  end
end
