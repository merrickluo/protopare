class CreateSpells < ActiveRecord::Migration[5.2]
  def change
    create_table :spells do |t|
      t.integer :level, index: true
      t.string :name, index: true
      t.string :name_en, index: true
      t.string :school, index: true
      t.string :casting_time
      t.string :range
      t.string :component
      t.string :duration
      t.boolean :concentration, index: true
      t.string :save_throw
      t.boolean :ritual, index: true
      t.string :materials
      t.string :description
      t.string :upgrade

      t.timestamps
    end
  end
end
