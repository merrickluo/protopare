class AddContentToSpecialities < ActiveRecord::Migration[5.2]
  def change
    add_column :specialities, :content, :text
  end
end
