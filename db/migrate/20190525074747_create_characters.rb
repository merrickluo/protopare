class CreateCharacters < ActiveRecord::Migration[5.2]
  def change
    create_table :characters do |t|
      t.string :name
      t.string :name_en
      t.references :race, foreign_key: true
      t.references :klass, foreign_key: true

      t.timestamps
    end
  end
end
