class CreateJoinTableKlassesSpells < ActiveRecord::Migration[5.2]
  def change
    create_join_table :klasses, :spells do |t|
      t.index [:klass_id, :spell_id]
      t.index [:spell_id, :klass_id]
    end
  end
end
