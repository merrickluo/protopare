# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_25_081256) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "characters", force: :cascade do |t|
    t.string "name"
    t.string "name_en"
    t.bigint "race_id"
    t.bigint "klass_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["klass_id"], name: "index_characters_on_klass_id"
    t.index ["race_id"], name: "index_characters_on_race_id"
  end

  create_table "characters_spells", id: false, force: :cascade do |t|
    t.bigint "character_id", null: false
    t.bigint "spell_id", null: false
    t.index ["character_id", "spell_id"], name: "index_characters_spells_on_character_id_and_spell_id"
    t.index ["spell_id", "character_id"], name: "index_characters_spells_on_spell_id_and_character_id"
  end

  create_table "klasses", force: :cascade do |t|
    t.string "name"
    t.string "name_en"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_klasses_on_name"
    t.index ["name_en"], name: "index_klasses_on_name_en"
  end

  create_table "klasses_spells", id: false, force: :cascade do |t|
    t.bigint "klass_id", null: false
    t.bigint "spell_id", null: false
    t.index ["klass_id", "spell_id"], name: "index_klasses_spells_on_klass_id_and_spell_id"
    t.index ["spell_id", "klass_id"], name: "index_klasses_spells_on_spell_id_and_klass_id"
  end

  create_table "races", force: :cascade do |t|
    t.string "name"
    t.string "name_en"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "specialities", force: :cascade do |t|
    t.string "name"
    t.string "name_en"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "content"
  end

  create_table "spells", force: :cascade do |t|
    t.integer "level"
    t.string "name"
    t.string "name_en"
    t.string "school"
    t.string "casting_time"
    t.string "range"
    t.string "component"
    t.string "duration"
    t.boolean "concentration"
    t.string "save_throw"
    t.boolean "ritual"
    t.string "materials"
    t.string "description"
    t.string "upgrade"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["concentration"], name: "index_spells_on_concentration"
    t.index ["level"], name: "index_spells_on_level"
    t.index ["name"], name: "index_spells_on_name"
    t.index ["name_en"], name: "index_spells_on_name_en"
    t.index ["ritual"], name: "index_spells_on_ritual"
    t.index ["school"], name: "index_spells_on_school"
  end

  add_foreign_key "characters", "klasses"
  add_foreign_key "characters", "races"
end
